<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 26.1.15
 * Time: 20.07
 */

namespace Soil\EventProcessorBundle\Command;

use EasyRdf\Http\Exception;
use Monolog\Logger;
use Soil\EventProcessorBundle\Processor\Selector\ProcessorSelector;
use Soil\RDFProcessorBundle\Service\EndpointClient;
use Soil\RDFProcessorBundle\Service\RDFProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CheckQueue2Command extends CommandAbstract    {



    /**
     * @var RDFProcessor
     */
    protected $rdfProcessor;

    /**
     * @var ProcessorSelector
     */
    protected $processorSelector;

    protected $queueStreamName;

    public function __construct($rdfProcessor, ProcessorSelector $processorSelector, $queueStreamName)   {
        $this->rdfProcessor = $rdfProcessor;
        $this->processorSelector = $processorSelector;
        $this->queueStreamName = $queueStreamName;

        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('events:check')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $worker = new \GearmanWorker();
        $worker->addServer();


        $this->logger->addInfo('Config info:');
        foreach ($this->configInfo as $name => $value)  {
            $this->logger->addInfo($name . ': ' . $value);
        }

        $this->logger->addInfo('Queue stream name used: ' . $this->queueStreamName);


        $logger = $this->logger;
        $worker->addFunction($this->queueStreamName, function ($job) use ($logger)
        {
            try {
                $this->logger->addInfo('Start worker');

                $workload = $job->workload();
                $this->logger->addInfo($workload);

                $entities = $this->rdfProcessor->processRDFChunk($workload);

                $graph = $this->rdfProcessor->getLastGraph();

                $this->logger->addInfo($graph->dump('text'));

                foreach ($entities as $entity) {
                    $processor = $this->processorSelector->selectProcessorForEvent($entity);

                    if ($processor) {
                        $this->logger->addInfo("Used " . get_class($processor) . ' for entity ' . $entity->type());
                        $processor->process($entity);
                    } else {
                        $this->logger->addInfo('Missing processor for for entity ' . $entity->type());
                    }

                }
            }
            catch (\Exception $e)    {
                $this->logger->addError($e->getMessage());
                $this->logger->addError($e->getTraceAsString());
                $job->sendException($e);
            }
        });

        $this->logger->addInfo('Start daemon');

        while (true)
        {
            $worker->work();
            if ($worker->returnCode() != GEARMAN_SUCCESS) break;
        }
    }


} 