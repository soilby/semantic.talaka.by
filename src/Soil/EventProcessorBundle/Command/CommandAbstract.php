<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 19.7.15
 * Time: 22.45
 */

namespace Soil\EventProcessorBundle\Command;


use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Console\Command\Command;

class CommandAbstract extends Command  {


    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var array
     */
    protected $configInfo = [];


    public function setLogger($logger)  {
        $this->logger = $logger;
    }


    public function setConfigInfo($name, $value)    {
        $this->configInfo[$name] = $value;
    }
} 