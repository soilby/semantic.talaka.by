<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 26.1.15
 * Time: 20.07
 */

namespace Soil\EventProcessorBundle\Command;

use EasyRdf\Http\Exception;
use Soil\RDFProcessorBundle\Service\EndpointClient;
use Soil\RDFProcessorBundle\Service\RDFProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class QueryCollectionCommand extends Command  {

    /**
     * @var EndpointClient
     */
    protected $endpointClient;

    public function __construct($rdfProcessor)   {
        $this->endpointClient = $rdfProcessor;
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('events:query')
            ->setDescription('Examine tripple store')
            ->addArgument(
                'task',
                InputArgument::REQUIRED,
                'Specify task'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {


            switch ($input->getArgument('task'))    {
                case 'get-all':
                    $this->endpointClient->getAllCreateEvents();

                    break;

                case 'put':
                    $this->endpointClient->putEvent();

                    break;

                default:
                    $output->writeln('Wrong argument');


            }


        }
        catch(Exception $e) {
            echo $e->getBody();
        }
    }
} 