<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 18.47
 */

namespace Soil\EventProcessorBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class EventProcessorCompilerPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('soil_event_processor.processor_selector')) {
            return;
        }

        $definition = $container->getDefinition(
            'soil_event_processor.processor_selector'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'event.processor'
        );
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addEventProcessor',
                array(new Reference($id))
            );
        }
    }
} 