<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\SemanticRepositoryModelBundle\Model\SubscriptionModel;

class CommentEventProcessor implements EventProcessorInterface {


    /**
     * @var SubscriptionModel
     */
    protected $subscriptionModel;

    protected $notifier;

    /**
     * @var Logger
     */
    protected $logger;


    public function __construct($subscriptionModel, $notifier)    {
        $this->subscriptionModel = $subscriptionModel;
        $this->notifier = $notifier;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:CommentEvent';
    }


    public function process(EasyRdfResource $event) {

        $this->logger->addInfo('Start process Comment Event');

        /*
         * получаем информацию о подписках на эту сущность
         * отправляем нотификации
         */

        $commentedEntity = $event->get('tal:relatedObject');
        if (!$commentedEntity) throw new \Exception('Commented entity is not setup');

        $subscribedEntityURI = $commentedEntity->getURI();
        $commentURI = $event->get('tal:target')->getURI();
        $commentAuthorURI = $event->get('tal:agent')->getURI();

        $result = $this->subscriptionModel->getSubscriptionsForEntity($subscribedEntityURI);

        $this->logger->addInfo('Subscriptions count: ' . count($result));

        foreach ($result as $subscription)   {
            $subscriberAgentURI = $subscription['agent'];

            $this->notifier->notify('CommentNotification', $subscriberAgentURI, [
                'entity' => $subscribedEntityURI,
                'comment' => $commentURI,
                'author' => $commentAuthorURI
            ]);

        }

    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }


} 