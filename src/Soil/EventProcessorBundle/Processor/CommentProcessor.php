<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\RdfNamespace;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\DiscoverBundle\Service\Resolver;
use Soil\RdfPersistenceBundle\Service\PersistenceService;
use Soil\RDFProcessorBundle\Service\EndpointClient;
use Soil\SemanticRepositoryModelBundle\Model\SubscriptionModel;

class   CommentProcessor implements EventProcessorInterface {

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var EndpointClient
     */
    protected $endpoint;

    /**
     * @var Resolver
     */
    protected $resolver;

    /**
     * @var PersistenceService
     */
    protected $persistenceService;


    public function __construct($endpoint, $resolver, $persistenceService)    {
        $this->endpoint = $endpoint;
        $this->resolver = $resolver;
        $this->persistenceService = $persistenceService;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:Comment';
    }


    public function process(EasyRdfResource $event) {
        $this->logger->addInfo('Start process Comment entity (not CommentEvent)');

        $commentedEntity = $event->get('tal:relatedObject');
        if (!$commentedEntity) throw new \Exception('Commented entity is not setup');

        $commentedEntityURI = $commentedEntity->getURI();


        $query = <<<EOT
    SELECT ?type ?author
    WHERE {
        <$commentedEntityURI> a ?type .

        OPTIONAL {
            <$commentedEntityURI> tal:author ?author .
        }
    }
    LIMIT 1
EOT;

        $this->logger->addInfo('Test for entity existence in semantic network');

        $result = $this->endpoint->query($query);

        $this->logger->addInfo($result->dump('text'));

        $result->rewind();
        $firstTriple = $result->current();

        $type = $firstTriple && $firstTriple->type instanceof EasyRdfResource ?
            $firstTriple->type->getUri() : null;

        if ($type) {
            $shorten = RdfNamespace::shorten($type);
        }
        else    {
            $shorten = null;
        }


        $this->logger->addInfo('Type: ' . print_r($type, true));
        $this->logger->addInfo('Shorten: ' . $shorten);

        switch (true)   {
            case is_null($type):
            case $shorten === 'tal:GenericTalakaEntity':
            case $shorten === 'owl:Thing':
                $entity = $this->resolver->getEntityForURI($commentedEntityURI, true);

                $this->logger->addInfo('Try to persist...');

                $this->persistenceService->persist($entity);

                break;
            default;
                //do nothing
                break;

        }

    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }


} 