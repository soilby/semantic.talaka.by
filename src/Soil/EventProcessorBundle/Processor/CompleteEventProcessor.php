<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\SemanticRepositoryModelBundle\Model\SubscriptionModel;

class CompleteEventProcessor implements EventProcessorInterface {


    protected $notifier;

    protected $resolver;

    /**
     * @var Logger
     */
    protected $logger;


    public function __construct($notifier)    {
        $this->notifier = $notifier;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:CompleteEvent';
    }


    public function process(EasyRdfResource $event) {

        $this->logger->addInfo('Start process Complete Event');


        $campaign = $event->get('tal:target');
        $campaignURI = $campaign->getURI();

        $result = $campaign->get('tal:result');

        $this->logger->addInfo("Campaign URI: `$campaignURI`");

        if (!$result)    {
            $this->logger->addWarning('Complete Event raised but tal:result is missing');
            return;
        }

        $result = \EasyRdf\RdfNamespace::shorten($result->getURI());
        if ($result !== 'tal:success')  {
            $this->logger->addInfo("Complete Event Raised with no success result `$result`. No notification will be sent");

            return;
        }

        $paymentLink = $campaign->get('tal:paymentLink');

        $promises = $campaign->all('tal:promise');

        $this->logger->addInfo('Promises count: ' . count($promises));

        foreach ($promises as $promise)   {
            $backer = $promise->get('tal:backer');
            if (!$backer) continue;
            $backerURI = $backer->getURI();

            $this->notifier->notify('CampaignCompleteNotification', $backerURI, [
                'entity' => $campaignURI,
                'paymentLink' => new Literal($paymentLink),
                'promiseURI' => new Literal($promise->getUri()),
                'promiseSum' => $promise->get('tal:promiseSum')->getValue(),
                'promiseDate' => $promise->get('tal:promiseDate')->getValue(),
            ]);

        }

    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param mixed $resolver
     */
    public function setResolver($resolver)
    {
        $this->resolver = $resolver;
    }




}