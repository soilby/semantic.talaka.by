<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\DiscoverBundle\Service\Resolver;
use Soil\EventProcessorBundle\Service\DeURInator;
use Soil\NotificationBundle\Service\Notification;
use Soil\SemanticHttpEndpointBundle\Model\SubscriptionModel;

class CreateEventProcessor implements EventProcessorInterface {


    /**
     * @var DeURInator
     */
    protected $deurinator;


    /**
     * @var Notification
     */
    protected $notifier;

    /**
     * @var Resolver
     */
    protected $resolver;

    /**
     * @var Logger
     */
    protected $logger;


    public function __construct($notifier, $deurinator)    {
        $this->notifier = $notifier;
        $this->deurinator = $deurinator;

    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:CreateEvent';
    }


    public function process(EasyRdfResource $event) {

        $this->logger->addInfo('Start process Create Event');

        $createdEntityURI = $event->get('tal:target');
        if (!$createdEntityURI) throw new \Exception('Nothing created? breaking.');

        $graph = $event->getGraph();
        $entityResource = $graph->getResource($event, 'tal:target');

        $this->logger->addInfo("Created entity: " . $createdEntityURI);

        $authorURI = $entityResource->get('tal:author');
        if (!$authorURI)    {
            throw new \Exception("Entity author is missing");
        }

        $host = parse_url($createdEntityURI, PHP_URL_HOST);

        if ($host) {
            $this->logger->addInfo('Host parsed: ' . $host);
        }
        else    {
            $host = 'www.talaka.by';
            $this->logger->addInfo('Host didn\'t parsed, default used: ' . $host);
        }


        $params = [
            'host' => $host
        ];


        $entityType = $entityResource->type();

        $this->logger->addInfo("Entity type: " . $entityType);

        switch ($entityType)   {
            case 'tal:IncubatorIdea':

                $this->notifier->notify('NewIdeaNotification', $authorURI, $params);
                break;

            default:
                $this->logger->addInfo('No instructions to process this kind of entities');
        }


    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param mixed $resolver
     */
    public function setResolver($resolver)
    {
        $this->resolver = $resolver;
    }


} 