<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 18.55
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Resource as EasyRdfResource;

interface EventProcessorInterface {


    public function support($type);

    public function process(EasyRdfResource $event);



} 