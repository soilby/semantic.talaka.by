<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\OnSiteNotificationBundle\Service\NotificationManager;

class PaidEventProcessor implements EventProcessorInterface {


    /**
     * @var NotificationManager
     */
    protected $notificationManager;

    protected $resolver;

    /**
     * @var Logger
     */
    protected $logger;


    public function __construct($notificationManager)    {
        $this->notificationManager = $notificationManager;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:PaidEvent';
    }


    public function process(EasyRdfResource $event) {

        $this->logger->addInfo('Start process Paid Event');

        $target = $event->get('tal:target');

        //treat as PROMISE

        $promiseURI = $target->getUri();

        $this->logger->addInfo('Target URI: ' . $promiseURI);
        $this->logger->addInfo('Treat it as PROMISE');

        $notifications = $this->notificationManager->getRepository()->findBy([
            'promiseURI' => $promiseURI
        ]);

        $notificationsCount = count($notifications);
        $this->logger->addInfo("Found `$notificationsCount` notifications");

        foreach ($notifications as $notification)   {
            $notification->setArchive(true);
        }

        $this->logger->addInfo('Persisting..');
        $this->notificationManager->flush();
        $this->logger->addInfo('Done');

    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param mixed $resolver
     */
    public function setResolver($resolver)
    {
        $this->resolver = $resolver;
    }




}