<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;

class RemindEventProcessor implements EventProcessorInterface {


    protected $notifier;

    protected $resolver;

    /**
     * @var Logger
     */
    protected $logger;


    public function __construct($notifier)    {
        $this->notifier = $notifier;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:RemindEvent';
    }


    public function process(EasyRdfResource $event) {

        $this->logger->addInfo('Start process Remind Event');


        $campaign = $event->get('tal:target');
        $campaignURI = $campaign->getURI();

        $paymentLink = $campaign->get('tal:paymentLink');

        $promises = $campaign->all('tal:promise');

        $this->logger->addInfo('Promises count: ' . count($promises));

        foreach ($promises as $promise)   {
            $backer = $promise->get('tal:backer');
            if (!$backer) continue;
            $backerURI = $backer->getURI();

            $this->notifier->notify('CampaignCompleteRemindNotification', $backerURI, [
                'entity' => $campaignURI,
                'paymentLink' => $paymentLink,
                'promiseURI' => new Literal($promise->getUri()),
                'promiseSum' => $promise->get('tal:promiseSum')->getValue(),
                'promiseDate' => $promise->get('tal:promiseDate')->getValue(),
            ]);

        }

    }

    /**
     * @param Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param mixed $resolver
     */
    public function setResolver($resolver)
    {
        $this->resolver = $resolver;
    }




}