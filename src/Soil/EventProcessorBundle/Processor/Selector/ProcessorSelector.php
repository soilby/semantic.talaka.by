<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 18.59
 */

namespace Soil\EventProcessorBundle\Processor\Selector;


use EasyRdf\Resource;
use Soil\EventProcessorBundle\Processor\EventProcessorInterface;

class ProcessorSelector {

    /**
     * @var EventProcessorInterface[]
     */
    protected $eventProcessors = [];

    public function addEventProcessor(EventProcessorInterface $processor)    {
        $this->eventProcessors[] = $processor;
    }

    public function selectProcessorForEvent(Resource $event)   {
        $types = $event->types();

        foreach ($types as $type)   {
            foreach ($this->eventProcessors as $processor)  {
                if ($processor->support($type)) {
                    return $processor;
                }
            }
        }

        return false;
    }
} 