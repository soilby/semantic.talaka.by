<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 16.06
 */

namespace Soil\EventProcessorBundle\Processor;


use EasyRdf\Graph;
use EasyRdf\Literal;
use EasyRdf\Resource as EasyRdfResource;
use Monolog\Logger;
use Soil\SemanticRepositoryModelBundle\Model\SubscriptionModel;

class SubscriptionEventProcessor implements EventProcessorInterface {

    /**
     * @var SubscriptionModel
     */
    protected $subscriptionModel;

    public function __construct($subscriptionModel)    {
        $this->subscriptionModel = $subscriptionModel;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function support($type)   {
        return $type === 'tal:SubscribeEvent' || $type === 'tal:UnsubscribeEvent';
    }

    protected function getSubscribeStatus(EasyRdfResource $event) {
        foreach ($event->types() as $type)  {
            switch (true) {
                case ($type === 'tal:SubscribeEvent'):
                    $status = true;
                    break;

                case ($type === 'tal:UnsubscribeEvent'):
                    $status = false;
                    break;

                default:
                    throw new \Exception("Provided event isn't supported. Use support method before call process");
            }

            return $status;
        }
    }


    public function process(EasyRdfResource $event) {
        $subscribeStatus = $this->getSubscribeStatus($event);

        $agentURI = $event->get('tal:agent')->getURI();
        $targetURI = $event->get('tal:target')->getURI();

        $this->logger->addInfo('process event ' . $event->type());
        $this->logger->addInfo('agent: ' . $agentURI);
        $this->logger->addInfo('target: ' . $targetURI);
        $this->logger->addInfo('status: ' . (string)(int)$subscribeStatus);

        if ($subscribeStatus)   {
            $this->subscriptionModel->subscribe($agentURI, $targetURI);
        }
        else    {
            $this->subscriptionModel->unsubscribe($agentURI, $targetURI);
        }

    }


    /**
     * @var Logger
     */
    protected $logger;
    public function setLogger($logger)  {
        $this->logger = $logger;
    }
} 