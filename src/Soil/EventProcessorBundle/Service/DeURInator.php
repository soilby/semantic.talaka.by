<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 5.2.15
 * Time: 0.39
 */

namespace Soil\EventProcessorBundle\Service;


use EasyRdf\RdfNamespace;
use Soil\DiscoverBundle\Entity\Comment;
use Soilby\EventComponent\Service\UrinatorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DeURInator implements ContainerAwareInterface {

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function setContainer(ContainerInterface $container = null)  {
        $this->container = $container;
    }

    public function parseUri($uri) {
        $iri = RdfNamespace::shorten($uri);

        if (!$iri)    return null;

        $colonPos = strpos($iri, ':');
        $namespace = substr($iri, 0, $colonPos);
        $uniquePart = substr($iri, $colonPos + 1);

        switch(true)    {
            case strpos($namespace, 'talcom') === 0:
                return [
                    'type' => $namespace,
                    'parsedType' => 'talcom',
                    'class' => null,
                    'id' => $uniquePart
                ];

            case strpos($namespace, 'talagent') === 0:
                return [
                    'type' => $namespace,
                    'parsedType' => 'talagent',
                    'class' => null,
                    'id' => $uniquePart
                ];

            case strpos($namespace, 'talidea') === 0:
                return [
                    'type' => $namespace,
                    'parsedType' => 'talidea',
                    'class' => null,
                    'id' => $uniquePart
                ];

            default:
                return [
                    'type' => $namespace,
                    'parsedType' => null,
                    'class' => null,
                    'id' => $uniquePart
                ];
        }
    }


    /**
     * @param $class
     *
     * @throws \Exception
     *
     *
     * @return Object
     */
    public function getEntityProvider($class) {

        switch ($class) {

            default:
                throw new \Exception("Class `$class` didn't supported");
        }

    }

    /**
     * @param $uri
     * @throws \Exception
     *
     * @return object
     */
    public function getEntity($uri) {
        $parsed = $this->parseUri($uri);

        if ($parsed && $parsed['class']) {
            $id = $parsed['id'];
            $class = $parsed['class'];

            $provider = $this->getEntityProvider($class);

            switch ($class) {
                default:
                    throw new \Exception("Class `$class` didn't supported");
            }
        }
        else    {
            throw new \Exception("Entity class didn't parsed. No fallback way.");

        }

    }


} 