<?php

namespace Soil\EventProcessorBundle;

use Soil\EventProcessorBundle\DependencyInjection\Compiler\EventProcessorCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SoilEventProcessorBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new EventProcessorCompilerPass());
    }
}
