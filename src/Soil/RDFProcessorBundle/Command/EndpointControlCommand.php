<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 26.1.15
 * Time: 20.07
 */

namespace Soil\RDFProcessorBundle\Command;

use Soil\EventProcessorBundle\Processor\EventProcessorInterface;
use Soil\EventProcessorBundle\Processor\Selector\ProcessorSelector;
use Soil\RDFProcessorBundle\Service\RDFProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class EndpointControlCommand extends Command  {

    protected $fusekiBin;
    protected $fusekiConfig;


    public function __construct($fusekiBin, $fusekiConfig)   {
        $this->fusekiBin = $fusekiBin;
        $this->fusekiConfig = $fusekiConfig;

        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setName('endpoint:control')
            ->setDescription('SPARQL endpoint control')
            ->addArgument(
                'cmd',
                InputArgument::REQUIRED,
                'start|stop'
            )
        ;
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        switch ($input->getArgument('cmd')) {
            case 'start':
                $fusekiDir = dirname($this->fusekiBin);
                chdir($fusekiDir);

                $command = $this->fusekiBin . ' --config=' . $this->fusekiConfig;
                $ret = `$command`;
                echo $ret;

                break;

            case 'stop':

                break;


        }
    }
} 