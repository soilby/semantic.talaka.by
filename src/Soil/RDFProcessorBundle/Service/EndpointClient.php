<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 29.1.15
 * Time: 8.38
 */

namespace Soil\RDFProcessorBundle\Service;



use EasyRdf\Graph;
use EasyRdf\Literal\DateTime;
use EasyRdf\Resource;
use EasyRdf\Sparql\Client;

class EndpointClient {

    /**
     * @var Client
     */
    protected $client;

    protected $namespacesConfig;

    public function __construct($client, $namespacesConfig)   {
        $this->client = $client;
        $this->namespacesConfig = $namespacesConfig;

        foreach ($this->namespacesConfig as $namespace => $uri) {
            \EasyRdf\RdfNamespace::set($namespace, $uri);
        }


    }

    public function query($sparql)  {
        return $this->client->query($sparql);
    }

    /**
     * @param $uri
     * @return Graph|\EasyRdf\Sparql\Result
     */
    public function getByURI($uri)  {
        $query = "

        SELECT * WHERE {

            <$uri> ?s ?o
        }

        ";

        $graph = $this->client->query($query);

        return $graph;

    }

    public function getAllCreateEvents()    {
        $query = "

        SELECT ?user WHERE {
            ?x tal:agent ?user .
            ?x a tal:CreateEvent .
            ?x tal:target <http://talaka.by.local/projects/550> .
        }

        ";

        $result = $this->client->query($query);
        echo $result->dump('text');
exit();

        $query2 = "SELECT * WHERE { talns:create-1 ?p ?o }";
        $result = $this->client->query($query2);

        foreach ($result as $element)   {
            var_dump($element   );
        }
    }

    protected function getEntityURI($id)   {

        return "http://semantic.talaka.by.local/entity/" . $id;
    }

    public function saveGraph(Graph $graph) {
        $res = $this->client->insert($graph);

        return $res;
    }

    public function putEvent()  {
        $graph = $this->createEvent('tal:CreateEvent', []);
        $res = $this->client->insert($graph);

        var_dump($res);

    }

    public function createEvent($eventClass, $params = [])   {

        $graph = new Graph('http://semantic.talaka.by.local/entity/');

        $eventUniqueCode = (string) new \MongoId();

        $uri = $this->getEntityURI('event_' . $eventUniqueCode);

        $event = $graph->resource($uri, $eventClass);
        $event->set('tal:date', new DateTime());


        return $graph;
    }
} 