<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 28.1.15
 * Time: 8.56
 */

namespace Soil\RDFProcessorBundle\Service;


use EasyRdf\Graph;
use EasyRdf\Parser;
use EasyRdf\Resource;

class RDFProcessor {

    /**
     * @var EndpointClient
     */
    protected $sparqlClient;
    protected $baseURI;

    protected $lastGraph;

    public function __construct($sparqlClient, $baseURI)   {
        $this->sparqlClient = $sparqlClient;
        $this->baseURI = $baseURI;

        \EasyRdf\RdfNamespace::set('rdfs', 'http://www.w3.org/2000/01/rdf-schema#');

    }


    /**
     * @param $rdf
     * @throws \EasyRdf\Exception
     *
     * @return \EasyRdf\Resource[]
     */
    public function processRDFChunk($rdf)   {

        $graph = new Graph($this->baseURI);
        $graph->parse($rdf, 'guess', $this->baseURI);

        $res = $this->sparqlClient->saveGraph($graph);

        $entities = $graph->resourcesMatching('a');

        $this->lastGraph = $graph;

        return $entities;

    }

    /**
     * @return mixed
     */
    public function getLastGraph()
    {
        return $this->lastGraph;
    }



}