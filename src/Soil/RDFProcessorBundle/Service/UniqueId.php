<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10.2.15
 * Time: 19.06
 */

namespace Soil\RDFProcessorBundle\Service;


class UniqueId {
    protected $id;

    public function __construct($prefix, $base = null)   {
        if (!$base) $base = time();
        $this->id = $prefix . '_' . sha1($base);
    }

    public function __toString()    {
        return $this->id;
    }
} 