<?php

namespace Soil\SemanticHttpEndpointBundle\Controller;

use Soil\SemanticHttpEndpointBundle\Model\ImportantForMeModel;
use Soil\SemanticHttpEndpointBundle\Model\JoinedModel;
use Soil\SemanticHttpEndpointBundle\Model\SubscriptionModel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SubscribeController
{
    protected $subscriptionModel;
    protected $importantForMeModel;
    protected $joinedModel;

    public function __construct(SubscriptionModel $subscriptionModel, ImportantForMeModel $importantForMeModel, JoinedModel $joinedModel)   {
        $this->subscriptionModel = $subscriptionModel;
        $this->importantForMeModel = $importantForMeModel;
        $this->joinedModel = $joinedModel;
    }

    public function getComradeListAction($agentURI) {
        $list = $this->subscriptionModel->getComradeListForAgent($agentURI);

        $comrades = [];

        foreach ($list as $result)    {
            $comrades[] = $result->comrade->getURI();
        }

        return new JsonResponse($comrades);
    }


    public function setAction(Request $request) {
        try {

//            $rawData = $request->getContent();
//            $data = json_decode($rawData, true);
//            if (!$data) {
//                throw new \Exception('Request malformed');
//            }

            $agentURI = $request->get('agent');
            $entityURI = $request->get('target');
            $status = $request->get('status');
            $type = $request->get('type', 'subscription');


            switch ($type)  {
                case 'subscription':
                    $model = $this->subscriptionModel;
                    break;
                case 'important':
                    $model = $this->importantForMeModel;
                    break;
                case 'joined':
                    $model = $this->joinedModel;
                    break;

            }



            if ($status)    {
                $model->subscribe($agentURI, $entityURI);
            }
            else    {
                $model->unsubscribe($agentURI, $entityURI);
            }

            return new JsonResponse([
                'success' => true,
                'status' => $status,
                'type' => $type
            ]);


        }
        catch (\Exception $e)   {
            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }

    }

    public function checkAction(Request $request)   {
        try {
//            $rawData = $request->getContent();
//            $data = json_decode($rawData, true);
//            if (!$data) {
//                throw new \Exception('Request malformed');
//            }

//            $agentURI = $data['agentURI'];
//            $entityURI = $data['targetURI'];

            $agentURI = $request->get('agentURI');
            $entityURI = $request->get('targetURI');

            $status = $this->subscriptionModel->isSubscribed($agentURI, $entityURI);

            return new JsonResponse([
                'success' => true,
                'status' => $status
            ]);


        }
        catch (\Exception $e)   {
            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage()
            ], 500);
        }




    }
}
