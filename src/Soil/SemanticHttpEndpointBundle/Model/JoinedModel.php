<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10.2.15
 * Time: 7.48
 */

namespace Soil\SemanticHttpEndpointBundle\Model;


use EasyRdf\Literal;
use EasyRdf\ParsedUri;
use EasyRdf\RdfNamespace;
use EasyRdf\Resource;
use Soil\RDFProcessorBundle\Service\UniqueId;

class JoinedModel extends SubscriptionModel {

        protected $subscriptionMark = 'tal:hasMember';

} 