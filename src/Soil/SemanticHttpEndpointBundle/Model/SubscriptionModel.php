<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10.2.15
 * Time: 7.48
 */

namespace Soil\SemanticHttpEndpointBundle\Model;


use EasyRdf\Literal;
use EasyRdf\ParsedUri;
use EasyRdf\RdfNamespace;
use EasyRdf\Resource;
use Soil\RDFProcessorBundle\Service\UniqueId;

class SubscriptionModel extends AbstractModel {

    protected $subscriptionMark = 'tal:subscribedBy';

    public function getSubscriptionsForEntity($entityURI)    {
        $query = <<<QUERY
            SELECT ?subscriber
            WHERE {
                <$entityURI> $this->subscriptionMark ?subscriber.
            }
QUERY;
        $subscriptions = [];

        $result = $this->endpoint->query($query);

        foreach ($result as $triple)   {
            if ($triple->subscriber && $triple->subscriber instanceof Resource)    {
                $subscriptions[] = $triple->subscriber->getURI();
            }

        }

        return $subscriptions;
    }


    public function subscribe($agentURI, $entityURI)  {

        $query = <<<QUERY
            <$entityURI> $this->subscriptionMark <$agentURI>
QUERY;

        $graphIRI = 'tal:SubscriptionGraph';
        $graphURI = RdfNamespace::expand($graphIRI);

        echo $query;
        try {
            $this->endpoint->insert($query, $graphURI);
        }
        catch (\EasyRdf\Http\Exception $e)  {
            echo $e->getBody();
        }
    }

    public function unsubscribe($agentURI, $entityURI)  {

        $graphIRI = 'tal:SubscriptionGraph';

        $query = <<<DELETE
            DELETE WHERE {
                GRAPH $graphIRI {
                    <$entityURI> $this->subscriptionMark <$agentURI>
                }
            }
DELETE;
echo $query;
        try {
            $this->endpoint->update($query);
        }
        catch (\EasyRdf\Http\Exception $e)  {
            echo $e->getBody();
        }
    }

    public function getComradeListForAgent($agentURI)    {
        $query = <<<QUERY


        select DISTINCT ?comrade

        where {

         ?idea a tal:IncubatorIdea .

         ?idea tal:hasMember    ?comrade .

         ?idea tal:author <$agentURI> .

        FILTER (?comrade != <$agentURI>) .


        }

QUERY;

        $graph = $this->endpoint->query($query);


        return $graph;

    }

    public function isSubscribed($agentURI, $entityURI)  {
        $query = "

            SELECT ?agent
            WHERE {

                 <$entityURI> $this->subscriptionMark  ?agent .

                 FILTER ( ?agent = <$agentURI>) .

              }
        ";
        $graph = $this->endpoint->query($query);

        if (count($graph) === 0)    {
            return false;
        }


        return true;
    }
}