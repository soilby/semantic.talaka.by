<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.7.15
 * Time: 0.34
 */

namespace Soil\SemanticHttpEndpointBundle\SubscribersMiner;


use Soil\CommentsDigestBundle\Entity\CommentBrief;
use Soil\SemanticHttpEndpointBundle\Model\ImportantForMeModel;

class ImportantEntitiesMiner {


    /**
     * @var ImportantForMeModel
     */
    protected $importantForMeModel;

    public function __construct($importantForMeModel) {
        $this->importantForMeModel = $importantForMeModel;
    }

    public function mine(CommentBrief $commentBrief)    {

        $entityURI = $commentBrief->getEntity();

        $subscriptions = $this->importantForMeModel->getSubscriptionsForEntity($entityURI);

        return $subscriptions;

    }
} 