<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 20.7.15
 * Time: 0.34
 */

namespace Soil\SemanticHttpEndpointBundle\SubscribersMiner;


use Soil\CommentsDigestBundle\Entity\CommentBrief;
use Soil\SemanticHttpEndpointBundle\Model\SubscriptionModel;

class SubscribersMiner {

    /**
     * @var SubscriptionModel
     */
    protected $subscriptionModel;

    public function __construct($subscriptionModel) {
        $this->subscriptionModel = $subscriptionModel;
    }

    public function mine(CommentBrief $commentBrief)    {

        $entityURI = $commentBrief->getEntity();

        $subscriptions = $this->subscriptionModel->getSubscriptionsForEntity($entityURI);

        return $subscriptions;

    }
} 