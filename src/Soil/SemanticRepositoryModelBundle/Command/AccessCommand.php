<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10.2.15
 * Time: 8.30
 */

namespace Soil\SemanticRepositoryModelBundle\Command;


use Soil\EventProcessorBundle\Processor\EventProcessorInterface;
use Soil\EventProcessorBundle\Processor\Selector\ProcessorSelector;
use Soil\RDFProcessorBundle\Service\RDFProcessor;
use Soil\SemanticRepositoryModelBundle\Model\SubscriptionModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AccessCommand extends Command  {



    /**
     * @var \Soil\SemanticRepositoryModelBundle\Model\AbstractModel[]
     */
    protected $models;

    public function __construct()   {
        parent::__construct();
    }

    public function addModel($id, $model)    {
        $this->models[$id] = $model;
    }


    protected function configure()
    {
        $this
            ->setName('access:test')
            ->setDescription('Examine SPARQL model layer')
            ->addArgument(
                'model',
                InputArgument::OPTIONAL,
                'Model for test', 'soil_semantic_repository_model.model.subscription_model'
            )
        ;
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $modelName = $input->getArgument('model');
        $model = $this->models[$modelName];

        switch (true) {
            case $model instanceof SubscriptionModel:
                $model->subscribe('http://talaka.by.local/user/8118', 'http://talaka.by.local/incubator/idea/2');
//                $ret = $model->isSubscribed('http://talaka.by.local/user/8118', 'http://talaka.by.local/incubator/idea/2');
//                $model->unsubscribe('http://talaka.by.local/user/8118', 'http://talaka.by.local/incubator/idea/2');

        }



    }
}