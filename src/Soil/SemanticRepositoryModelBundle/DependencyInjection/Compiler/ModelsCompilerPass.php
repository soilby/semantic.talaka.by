<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 3.2.15
 * Time: 18.47
 */

namespace Soil\SemanticRepositoryModelBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ModelsCompilerPass implements CompilerPassInterface {

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('soil_semantic_repository_model.command.access_command')) {
            return;
        }

        $definition = $container->getDefinition(
            'soil_semantic_repository_model.command.access_command'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'soil_semantic_repository_model'
        );

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addModel',
                array($id, new Reference($id))
            );
        }
    }
} 