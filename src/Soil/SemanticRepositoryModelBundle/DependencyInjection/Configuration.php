<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 9.2.15
 * Time: 20.36
 */

namespace Soil\SemanticRepositoryModelBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface {
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()  {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('soil_semantic_repository_model');

        $rootNode
            ->children()
            ->end();


        return $treeBuilder;
    }

} 