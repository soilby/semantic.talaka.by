<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 10.2.15
 * Time: 7.58
 */

namespace Soil\SemanticRepositoryModelBundle\Model;


abstract class AbstractModel {

    /**
     * @var \EasyRdf\Sparql\Client
     */
    protected $endpoint;

    /**
     * @var array
     */
    protected $namespaces;

    public function __construct($endpoint, $namespaces)    {
        $this->endpoint = $endpoint;
        $this->namespaces = $namespaces;

        foreach ($namespaces as $namespace => $uri) {
            \EasyRdf\RdfNamespace::set($namespace, $uri);
        }
    }
} 