<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 21.2.15
 * Time: 8.46
 */

namespace Soil\SemanticRepositoryModelBundle\Model;


class EventsModel extends AbstractModel {

    public function getEvent($eventClass)   {
        $query = <<<QUERY

            PREFIX tal:<http://semantic.talaka.by/ns/talaka.owl#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            SELECT
                ?s ?d
            WHERE {
                ?s a tal:Event .
                ?s tal:date ?d
            FILTER (
                ?d > "2015-02-13T17:07:48+03:00"^^xsd:dateTime &&
                ?d > "2015-02-14T22:39:09+03:00"^^xsd:dateTime

              )

            }
QUERY;

        $result = $this->endpoint->query($query);
    }
} 