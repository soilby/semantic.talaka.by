<?php
/**
 * Created by PhpStorm.
 * User: fliak
 * Date: 9.2.15
 * Time: 20.34
 */

namespace Soil\SemanticRepositoryModelBundle;


use Soil\SemanticRepositoryModelBundle\DependencyInjection\Compiler\ModelsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SoilSemanticRepositoryModelBundle extends Bundle {

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ModelsCompilerPass());
    }
} 